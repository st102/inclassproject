

#ifndef Deck_hpp
#define Deck_hpp

#include "Card.hpp"
#include <vector>

class Deck
{
private:
    std::vector<Card> m_deck;
    int m_topcard;
    
public:
    Deck();
    void Shuffle();
    bool IsEmpty();
    Card Draw();
};

#endif /* Deck_hpp */
