
#ifndef Player_hpp
#define Player_hpp

#include "Card.hpp"
#include "Deck.hpp"
#include <vector>

class Player
{
private:
    int m_score;
    std::vector<Card> m_cards;
    int m_drawcard;
    
public:
    Player(Deck&);
    Card Draw();
    void PickCard(Deck&);
    int Score() const;
    void AddPoints(int point);
};

#endif /* Player_hpp */
