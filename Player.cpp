
#include "Player.hpp"
#include "Card.hpp"
#include "Deck.hpp"
#include <cstdlib>
#include <ctime>

Player::Player(Deck& deck)
{
    m_score = 0;
    for (int i = 0; i<26; i++)
    {
        m_cards[i] = deck.Draw();
    }
    m_drawcard = 0;
}

void Player::PickCard(Deck& deck)
{
    m_cards[m_drawcard] = deck.Draw();
}

int Player::Score() const
{
    return m_score;
}

void Player::AddPoints(int point)
{
    m_score += point;
}

Card Player::Draw()
{
    m_drawcard = rand() % 26;
    return m_cards[m_drawcard];
}
