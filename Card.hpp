
#ifndef Card_hpp
#define Card_hpp

#include <iostream>
#include <vector>

enum Suit
{
    Spades,
    Clubs,
    Diamonds,
    Hearts
};

enum Rank
{
    Ace,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
};

class Card
{
    
private:
    Rank m_rank;
    Suit m_suit;
    
public:
    Card();
    Card(Rank, Suit);
    
    friend bool operator<(const Card&, const Card&);
    friend bool operator>(const Card&, const Card&);
    friend bool operator==(const Card&, const Card&);
    friend std::ostream& operator<<(std::ostream&, const Card&);
};

#endif /* Card_hpp */
