//
//  main.cpp
//  TheGameofWar
//
//  Sagar Timsina.
//

#include <iostream>
#include "Player.hpp"
#include "Deck.hpp"

int main() {
    
    Deck deck;
    deck.Shuffle();
    
    Player player1(deck);
    Player player2(deck);

    while (!deck.IsEmpty())
    {
        Card card1 = player1.Draw();
        std::cout << "Player 1 :" << card1 << std::endl;
        
        Card card2 = player2.Draw();
        std::cout << "Player 2 :" << card2 << std::endl;
        
        if(card1 > card2)
        {
            player1.AddPoints(1);
        }
        else if(card1 < card2)
        {
            player2.AddPoints(1);
        }
        else if (card1 == card2)
        {
            player1.AddPoints(1);
            player2.AddPoints(1);
        }
        
        player1.PickCard(deck);
        player2.PickCard(deck);
    }
    
    std::cout << "Scores: Player 1: " << player1.Score() << " Player 2: " << player2.Score() << std::endl;
    
    return 0;
}
