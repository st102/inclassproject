
#include "Deck.hpp"
#include "Card.hpp"
#include <algorithm>

Deck::Deck()
{
    m_topcard = 52;
    Card s0 {Ace, Spades};
    Card s1 {Two, Spades};
    Card s2 {Three, Spades};
    Card s3 {Four, Spades};
    Card s4 {Five, Spades};
    Card s5 {Six, Spades};
    Card s6 {Seven, Spades};
    Card s7 {Eight, Spades};
    Card s8 {Nine, Spades};
    Card s9 {Ten, Spades};
    Card s10 {Jack, Spades};
    Card s11 {Queen, Spades};
    Card s12 {King, Spades};
    
    m_deck.push_back(s1);
    m_deck.push_back(s2);
    m_deck.push_back(s3);
    m_deck.push_back(s4);
    m_deck.push_back(s5);
    m_deck.push_back(s6);
    m_deck.push_back(s7);
    m_deck.push_back(s8);
    m_deck.push_back(s9);
    m_deck.push_back(s10);
    m_deck.push_back(s11);
    m_deck.push_back(s12);
    
    Card c0 {Ace, Clubs};
    Card c1 {Two, Clubs};
    Card c2 {Three, Clubs};
    Card c3 {Four, Clubs};
    Card c4 {Five, Clubs};
    Card c5 {Six, Clubs};
    Card c6 {Seven, Clubs};
    Card c7 {Eight, Clubs};
    Card c8 {Nine, Clubs};
    Card c9 {Ten, Clubs};
    Card c10 {Jack, Clubs};
    Card c11 {Queen, Clubs};
    Card c12 {King, Clubs};
    
    m_deck.push_back(c1);
    m_deck.push_back(c2);
    m_deck.push_back(c3);
    m_deck.push_back(c4);
    m_deck.push_back(c5);
    m_deck.push_back(c6);
    m_deck.push_back(c7);
    m_deck.push_back(c8);
    m_deck.push_back(c9);
    m_deck.push_back(c10);
    m_deck.push_back(c11);
    m_deck.push_back(c12);


    Card d0 {Ace, Diamonds};
    Card d1 {Two, Diamonds};
    Card d2 {Three, Diamonds};
    Card d3 {Four, Diamonds};
    Card d4 {Five, Diamonds};
    Card d5 {Six, Diamonds};
    Card d6 {Seven, Diamonds};
    Card d7 {Eight, Diamonds};
    Card d8 {Nine, Diamonds};
    Card d9 {Ten, Diamonds};
    Card d10 {Jack, Diamonds};
    Card d11 {Queen, Diamonds};
    Card d12 {King, Diamonds};
    
    m_deck.push_back(d1);
    m_deck.push_back(d2);
    m_deck.push_back(d3);
    m_deck.push_back(d4);
    m_deck.push_back(d5);
    m_deck.push_back(d6);
    m_deck.push_back(d7);
    m_deck.push_back(d8);
    m_deck.push_back(d9);
    m_deck.push_back(d10);
    m_deck.push_back(d11);
    m_deck.push_back(d12);
    
    Card h0 {Ace, Hearts};
    Card h1 {Two, Hearts};
    Card h2 {Three, Hearts};
    Card h3 {Four, Hearts};
    Card h4 {Five, Hearts};
    Card h5 {Six, Hearts};
    Card h6 {Seven, Hearts};
    Card h7 {Eight, Hearts};
    Card h8 {Nine, Hearts};
    Card h9 {Ten, Hearts};
    Card h10 {Jack, Hearts};
    Card h11 {Queen, Hearts};
    Card h12 {King, Hearts};
    
    m_deck.push_back(h1);
    m_deck.push_back(h2);
    m_deck.push_back(h3);
    m_deck.push_back(h4);
    m_deck.push_back(h5);
    m_deck.push_back(h6);
    m_deck.push_back(h7);
    m_deck.push_back(h8);
    m_deck.push_back(h9);
    m_deck.push_back(h10);
    m_deck.push_back(h11);
    m_deck.push_back(h12);
}

void Deck::Shuffle()
{
    std::random_shuffle(m_deck.begin(), m_deck.end());
}

bool Deck::IsEmpty()
{
    return m_topcard <= 0;
}

Card Deck::Draw()
{
    return m_deck[m_topcard];
}
