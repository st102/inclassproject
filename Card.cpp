
#include "Card.hpp"

Card::Card()
:m_rank(Ace), m_suit(Spades)
{
}

Card::Card(Rank r, Suit s)
:m_rank(r), m_suit(s)
{
}

bool operator<(const Card& c1, const Card& c2)
{
    return c1.m_rank < c2.m_rank;
}

bool operator>(const Card& c1, const Card& c2)
{
    return c1.m_rank > c2.m_rank;
}

bool operator==(const Card& c1, const Card& c2)
{
    return c1.m_rank == c2.m_rank;
}

std::ostream& operator<<(std::ostream& out, const Card& card)
{
    switch (card.m_rank) {
        case Ace:
            out << "Ace";
            break;
        case Two:
            out << "Two";
            break;
        case Three:
            out << "Three";
            break;
        case Four:
            out << "Four";
            break;
        case Five:
            out << "Five";
            break;
        case Six:
            out << "Six";
            break;
        case Seven:
            out << "Seven";
            break;
        case Eight:
            out << "Eight";
            break;
        case Nine:
            out << "Nine";
            break;
        case Ten:
            out << "Ten";
            break;
        case Jack:
            out << "Jack";
            break;
        case Queen:
            out << "Queen";
            break;
        case King:
            out << "King";
            break;
            
        default:
            out << card.m_rank;
            break;
    }
    
    switch (card.m_suit) {
        case Diamonds:
            out << " of Diamonds";
            break;
            
        case Spades:
            out << " of Spades";
            break;
            
        case Hearts:
            out << " of Hearts";
            break;
        case Clubs:
            out << " of Clubs";
            break;
    
        default:
            out << card.m_suit;
            break;
    }
    
    return out;
}
